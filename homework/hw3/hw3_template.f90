!Module containing routines for differentiating array of size N with
!2nd order and 4th order compact finite differences
!Test functions apply these methods to a Gaussian function and
!return performance information.

module fdmodule
    implicit none


    save

contains
!-------------------
subroutine fd2(f,df)
    !2nd order centered finite difference
    !Header variables
    !f: function to be differentiated
    !df: derivative of f
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df


end subroutine fd2
!-----------------

subroutine cfd4(f,df)
    !compact 4th order centered finite difference
    !Header variables
    !f: function to be differentiated
    !df: derivative of f
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df

end subroutine cfd4
!------------------

subroutine test_fd(alpha,error)
    !test accuracy of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    implicit none
    integer :: i1
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2)

end subroutine test_fd
!---------------------

subroutine test_fd_time(alpha,error,time)
    !test accuracy and speed of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    !time: 2-element array containing wall-clock time required by 1000 calls
    !to fd2 and cfd4
    implicit none
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2),time(2)


end subroutine test_fd_time
!---------------------------
end module fdmodule















