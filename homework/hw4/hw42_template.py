"""Solve advection equation with odeint and fortran fd2 routine in fdmodule


"""    
    
from fd import fdmodule as f1 #you may change this line if desired
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint



def advection1(tf,n,dx,c=1.0,S=0.0,display=False):
    """solve advection eqn, df/dt + c df/dx = S 
    where c and S are constants, x=0,dx,...(N-1)*dx,
    and f is periodic, f(x_i) = f(x_i+N*dx).
    Initial condition: f(x,t=0) = exp(-100.0*(x-x0)**2)
    Returns: f(x,tf)
    """
    
    #function provides RHS to odeint
    def RHS(f,t,dx,n,c,S):
    
    
  
def test_advection1():
    
    
                                                                               
#This section will be used for assessment, you may enter a call to fdtest_eff, but
#the three un-commented lines should be left as is in your final submission.    
if __name__ == "__main__":
    n = 1000
    dx = 1.0/float(n-1)
    f = advection1(2,n,dx,1,1,True)
    m = test_advection1()
    plt.show()
