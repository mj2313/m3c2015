"""Name
8-digit college id
"""

#import modules here


#---------------------
def test_randn(n,m,s):
    """Input variables:
    n   number of normal random variables to be generated
    m   random variables generated with mean = m
    s   random variables generated with standard deviation = s
    """
        
 
#---------------------
def wiener1(dt,X0,Nt):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    """
    
    
#---------------------
def wienerM(dt,X0,Nt,M):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    M     number of samples
    """


#-----------------------------
def langevin1(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    
#-----------------------------
def langevinM(dt,V0,Nt,M,g=1.0,debug=False):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    M     number of samples
    g     friction factor, gamma
    debug flag to control plotting of figures
    """    

#--------------------------------
def langevinTest(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
#---------------------------------
#The section below is included for assessment and *must* be included exactly as below in the final
#file that you submit, but you may want to omit it (e.g. comment it out) while you are developing
#your code.  
if __name__ == "__main__":
    npr.seed(1)
    test_randn(1000)
    wiener1(0.1,0.0,1000)
    wienerM(0.1,2.0,1000,20000)
    langevin1(0.1,0.0,1000,0.01)
    print "Error from LangevinM, dt=%1.2f,V0=%1.2f,Nt=%d,M=%d,g=%1.3f: "%(0.1,1.0,1000,2000,0.05), langevinM(0.1,1.0,1000,2000,0.05,True)
    print "Convergence rate for Langevin Monte Carlo calculations from langevinTest", langevinTest(0.1,1.0,1000,0.25)
    plt.show()