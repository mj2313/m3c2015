# -*- coding: utf-8 -*-
"""Name Maros Janco 
8-digit college id 00866730
"""

#import modules here
import numpy as np
import matplotlib.pyplot as plt
import numpy.random as npr
#---------------------
def test_randn(n,m=0,s=1):
    """Input variables:
    n   number of normal random variables to be generated
    m   random variables generated with mean = m
    s   random variables generated with standard deviation = s
    """
    samples = m + s*npr.randn(n)
    plt.figure()
    plt.hist(samples)
    plt.title('Test_randn, Maros Janco')
    
#---------------------
def wiener1(dt=0.1,X0=0,Nt=10):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    """
    dX = np.r_[X0, np.sqrt(dt)*npr.randn(Nt)]
    X = np.cumsum(dX)
    x_st = np.linspace(0,dt*Nt,dt*Nt*100+1)
    y_st = np.sqrt(x_st)
    plt.figure()
    plt.plot(np.arange(0,dt*(Nt+1),dt),X, color='blue')
    plt.title('Wiener1, Maros Janco, Wiener process with dt=%s, Nt=%s'%(dt, Nt))
    plt.plot(x_st,y_st+X0, color = 'black', linestyle = '--')
    plt.plot(x_st,-y_st+X0, color = 'black', linestyle ='--')
    plt.xlabel('t')
    plt.ylabel('X(t)')
    
#---------------------
def wienerM(dt,X0,Nt,M):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    M     number of samples
    """
    dX = np.concatenate((np.ones((M,1))*X0,np.sqrt(dt)*npr.randn(M,Nt)), axis=1)
    X = np.cumsum(dX, axis=1) #X is a M x (Np+1) matrix with each row representing one possible Wiener process
    Mean = np.mean(X, axis=0, dtype=np.float64) #np.float64 chosen for accuracy
    Std = np.std(X, axis=0, dtype=np.float64) #np.float64 chosen for accuracy
    x_st = np.linspace(0,dt*Nt,dt*Nt*100+1)
    y_st = np.sqrt(x_st)
    plt.figure()
    plt.plot(np.arange(0,dt*(Nt+1),dt),Mean, label="computed mean", color='red')
    plt.plot(np.arange(0,dt*(Nt+1),dt),Std, label="computed standard deviation", color='blue')
    plt.title('WienerM, Maros Janco, the mean and std of %s \n Wiener processes average with dt=%s, Nt=%s'%(M, dt, Nt))
    plt.plot(x_st,y_st, color = 'black',  label="analytical standard deviation", linestyle = '--')
    plt.plot(x_st,-y_st, color = 'black', linestyle ='--')
    plt.plot(x_st,X0*np.ones(len(x_st)), label="analytical mean", color = 'green', linestyle ='-')
    plt.xlabel('t')
    plt.ylabel('mean and std')
    plt.legend(bbox_to_anchor=(0.52,1), prop={'size':12})
    
#-----------------------------
def langevin1(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    V = np.zeros(Nt+1)
    V[0] = V0
    for t in range(Nt):
        V[t+1] = V[t] - g*V[t]*dt +np.sqrt(dt)*npr.randn()
    
    plt.figure()
    plt.plot(np.arange(0,dt*(Nt+1),dt),V, color='purple')
    plt.title('langevin1, Maros Janco, Langevin Equation with $\gamma$=%s dt=%s, $N_t$=%s'%(g, dt, Nt))
    plt.xlabel('t')
    plt.ylabel('V(t)')
    
#-----------------------------
def langevinM(dt,V0,Nt,M,g=1.0,debug=False):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    M     number of samples
    g     friction factor, gamma
    debug flag to control plotting of figures
    """    
    V = np.ones((M,Nt+1))*V0
    for i in range(M):
        for t in range(Nt):
            V[i,t+1] = V[i,t] - g*V[i,t]*dt +np.sqrt(dt)*npr.randn()
   
    Mean = np.mean(V, axis=0, dtype=np.float64) #np.float64 chosen for accuracy
    Var = np.var(V, axis=0, dtype=np.float64) #np.float64 chosen for accuracy
    t = np.linspace(0,dt*Nt,Nt+1)
    mu_a = V0*np.exp(-g*t)
    eps = (1.0/Nt)* (sum(abs(Mean-mu_a))) #can keep 0-th term of the sum as it is zero
   
    # bigger length for smoother graph of analytic mean and variance
    t_graph = np.linspace(0,dt*Nt,dt*Nt*100+1) 
    mu_graph = V0*np.exp(-g*t_graph)
    var_graph = (1-np.exp(-2*g*t_graph))/(2*g)
    
    if debug:
        plt.figure()
        plt.title('langevinM, Maros Janco, Langevin Equation %s-sample\nmean and variance with $\gamma$=%s, dt=%s, $V_0$=%s, $N_t$=%s'%(M, g, dt,V0,Nt))
        plt.plot(t_graph, mu_graph, label="Analytical mean", color = '#333300', linestyle ='-.')
        plt.plot(t, Mean, label="Computed mean", color = '#66CC00', linestyle ='-')
        plt.plot(t_graph, var_graph, label="Analytical variance", color = '#990033', linestyle ='--')
        plt.plot(t, Var, label="Computed variance", color = '#CC0033', linestyle ='-')
        #plt.ylim([-1,11])
        plt.xlabel('t')
        plt.ylabel('mean and variance comparisons')
        plt.legend(bbox_to_anchor=(1,0.60), prop={'size':12})
        return eps
    else:
        return eps
#--------------------------------
def langevinTest(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    M = np.power(10,range(1,5))
    err = np.zeros(len(M))
    for m in range(len(M)):
        err[m] = langevinM(dt,V0,Nt,M[m],g=1.0)    
    

    plt.figure()
    plt.plot(M, err, color='black', linestyle='-') #error against M on a log-log plot
    plt.title('LangevinTest, Maros Janco, Error vs. M with $\gamma$=%s, dt=%s, $V_0$=%s, $N_t$=%s'%(g, dt, V0, Nt))
    plt.xlabel('M')
    plt.ylabel('error')
    
    (est1_inv, est2) = np.polyfit(np.log(M),np.log(err),1)

    return  -est1_inv
#---------------------------------
#The section below is included for assessment and *must* be included exactly as below in the final
#file that you submit, but you may want to omit it (e.g. comment it out) while you are developing
#your code.  
if __name__ == "__main__":
    npr.seed(1)
    test_randn(1000)
    wiener1(0.1,0.0,1000)
    wienerM(0.1,2.0,1000,20000)
    langevin1(0.1,0.0,1000,0.01)
    print "Error from LangevinM, dt=%1.2f,V0=%1.2f,Nt=%d,M=%d,g=%1.3f: "%(0.1,1.0,1000,2000,0.05), langevinM(0.1,1.0,1000,2000,0.05,True)
    print "Convergence rate for Langevin Monte Carlo calculations from langevinTest", langevinTest(0.1,1.0,1000,0.25)
    plt.show()
